/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import eu.andret.arguments.AnnotatedCommand;
import eu.andret.arguments.CommandManager;
import eu.andret.arguments.api.entity.FallbackConstants;
import eu.andret.ats.help.request.Request;
import eu.andret.ats.help.request.RequestManager;
import eu.andret.ats.help.request.RequestPlayer;
import eu.andret.ats.help.request.Status;
import eu.andret.ats.help.util.IdProvider;
import eu.andret.ats.help.util.OfflinePlayerAdapter;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

public class HelpPlugin extends JavaPlugin {
	private final Path requestsFile = Paths.get(getDataFolder().toString(), "requests.json");

	@NotNull
	private final IdProvider idProvider = new IdProvider();
	private final RequestManager requestManager = new RequestManager(this);
	private final Gson gson = new GsonBuilder()
			.registerTypeHierarchyAdapter(OfflinePlayer.class, new OfflinePlayerAdapter(this))
			.setPrettyPrinting()
			.create();

	@Override
	public void onEnable() {
		saveDefaultConfig();
		load();
		idProvider.setLastUsedId(requestManager.getLastUsedId());
		setupCommand();
		final int time = getConfig().getInt("backup-frequency", 0);
		if (time > 0) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::save, 20L * time, 20L * time);
		}
		new Metrics(this, 10699);
	}

	@Override
	public void onDisable() {
		save();
	}

	@NotNull
	public IdProvider getIdProvider() {
		return idProvider;
	}

	private void setupCommand() {
		final AnnotatedCommand<HelpPlugin> commandExecutor =
				CommandManager.registerCommand(HelpCommand.class, this, requestManager);
		commandExecutor.setOnUnknownSubCommandExecutionListener(sender ->
				sender.sendMessage("Unknown sub-command"));
		commandExecutor.setOnInsufficientPermissionsListener(sender ->
				sender.sendMessage(getMessage("noPerms")));

		commandExecutor.addArgumentCompleter("anyRequestId", requestManager.getPlayers()
				.stream()
				.map(RequestPlayer::requests)
				.flatMap(Collection::stream)
				.map(Request::getId)
				.map(String::valueOf)
				.toList());

		commandExecutor.addArgumentCompleter("pagesCount", Stream.iterate(1, i -> i + 1)
				.limit(requestManager.getRequestsCount() / 5 + 1)
				.map(String::valueOf)
				.toList());

		commandExecutor.addArgumentCompleter("pagesStatusCount", (ignoredSender, args) -> Stream.iterate(1, i -> i + 1)
				.limit(requestManager.getRequestsCount(requestManager.mapNameToStatus(new ArrayList<>(args).get(args.size() - 1))) / 5 + 1)
				.map(String::valueOf)
				.toList());

		commandExecutor.addTypeMapper(Status.class, requestManager::mapNameToStatus, FallbackConstants.ON_NULL);
		commandExecutor.addTypeCompleter(Status.class, Arrays.stream(Status.values())
				.map(Enum::name)
				.toList());
	}

	private void save() {
		try {
			if (!Files.exists(requestsFile)) {
				Files.createFile(requestsFile);
			}
			final JsonWriter jsonWriter = gson.newJsonWriter(new PrintWriter(requestsFile.toFile()));
			jsonWriter.setIndent("\t");
			gson.toJson(requestManager.getPlayers().toArray(new RequestPlayer[0]), RequestPlayer[].class, jsonWriter);
			jsonWriter.close();
		} catch (final IOException ex) {
			ex.printStackTrace();
		}
	}

	private void load() {
		try (final Reader reader = new FileReader(requestsFile.toFile())) {
			requestManager.getPlayers().addAll(Arrays.asList(gson.fromJson(reader, RequestPlayer[].class)));
			getLogger().info("Successfully loaded players' requests");
		} catch (final IOException ex) {
			Bukkit.getLogger().throwing(getClass().getName(), "load", ex);
		}
	}

	@NotNull
	String getMessage(@NotNull final String path) {
		final String string = getConfig().getString("messages." + path, "");
		final String colored = ChatColor.translateAlternateColorCodes('&', string);
		return String.format("%s%s", colored, ChatColor.RESET);
	}
}
