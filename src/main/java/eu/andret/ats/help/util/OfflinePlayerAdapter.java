/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import eu.andret.ats.help.HelpPlugin;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.UUID;

public class OfflinePlayerAdapter implements JsonSerializer<OfflinePlayer>, JsonDeserializer<OfflinePlayer> {
	@NotNull
	private final HelpPlugin plugin;

	public OfflinePlayerAdapter(@NotNull final HelpPlugin plugin) {
		this.plugin = plugin;
	}

	@NotNull
	@Override
	public JsonElement serialize(@NotNull final OfflinePlayer offlinePlayer, @NotNull final Type typeOfSrc, @NotNull final JsonSerializationContext context) {
		return new JsonPrimitive(offlinePlayer.getUniqueId().toString());
	}

	@NotNull
	@Override
	public OfflinePlayer deserialize(@NotNull final JsonElement json, @NotNull final Type typeOfT, @NotNull final JsonDeserializationContext context) throws JsonParseException {
		return plugin.getServer().getOfflinePlayer(UUID.fromString(json.getAsString()));
	}
}
