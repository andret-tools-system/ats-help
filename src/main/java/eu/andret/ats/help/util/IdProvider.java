/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.util;

/**
 * The class to easily provide an auto-incremented id.
 *
 * @author Andret2344
 * @since Feb 12, 2022
 */
public class IdProvider {
	private long lastUsedId;

	public IdProvider(final long lastUsedId) {
		this.lastUsedId = lastUsedId;
	}

	public IdProvider() {
		this(0);
	}

	/**
	 * Increments the stored value and returns it.
	 *
	 * @return The next value.
	 */
	public long next() {
		return ++lastUsedId;
	}

	public void setLastUsedId(final long lastUsedId) {
		this.lastUsedId = lastUsedId;
	}
}
