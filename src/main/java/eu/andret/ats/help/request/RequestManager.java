/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.request;

import eu.andret.ats.help.HelpPlugin;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * The facade class to get all required information about requests in an easy way.
 *
 * @author Andret2344
 * @since Dec 07, 2022
 */
public class RequestManager {
	@NotNull
	private final HelpPlugin plugin;
	private final List<RequestPlayer> players = new ArrayList<>();

	public RequestManager(@NotNull final HelpPlugin plugin) {
		this.plugin = plugin;
	}

	/**
	 * Creates the request of the given arguments and adds first free id.
	 *
	 * @param message The text of the request.
	 * @param status  The {@link Status} of the request.
	 * @return The fully created {@link Request} with an id.
	 */
	@NotNull
	public Request createRequest(@NotNull final String message, @NotNull final Status status) {
		return new Request(plugin.getIdProvider().next(), message, status);
	}

	/**
	 * Converts the status to its user-friendly String representation.
	 *
	 * @param status The status to be converted to a String.
	 * @return The String that contains the {@link Status#name()} in its color and the color reset at the end.
	 */
	@NotNull
	public String format(@NotNull final Status status) {
		return String.format("%s%s%s", status.getColor(), status.name(), ChatColor.RESET);
	}

	/**
	 * Converts the request to its user-friendly String representation.
	 *
	 * @param request The request to be converted to a String.
	 * @return The String that is formatted as {@code "#1: Request text [STATUS]"}.
	 */
	@NotNull
	public String format(@NotNull final Request request) {
		return String.format("%s#%d%s: %s [%s]",
				ChatColor.AQUA,
				request.getId(),
				ChatColor.RESET,
				request.getMessage(),
				format(request.getStatus()));
	}

	/**
	 * Finds the related {@link RequestPlayer} for the {@link OfflinePlayer}. Creates and stores the new one, if didn't
	 * find any.
	 *
	 * @param player The player to check.
	 * @return The found or newly created {@link RequestPlayer}.
	 */
	@NotNull
	public RequestPlayer getRequestPlayer(@NotNull final OfflinePlayer player) {
		final Optional<RequestPlayer> playerOptional = players.stream()
				.filter(requestPlayer -> requestPlayer.offlinePlayer().equals(player))
				.findAny();
		if (playerOptional.isPresent()) {
			return playerOptional.get();
		}

		final RequestPlayer requestPlayer = new RequestPlayer(player);
		players.add(requestPlayer);
		return requestPlayer;
	}

	/**
	 * Counts all requests of all players.
	 *
	 * @return The summarized count of all requests.
	 */
	public long getRequestsCount() {
		return players.stream()
				.map(RequestPlayer::requests)
				.mapToLong(Collection::size)
				.sum();
	}

	/**
	 * Counts all requests of all players of the given status.
	 *
	 * @return The summarized count of all requests with the given status.
	 */
	public long getRequestsCount(@Nullable final Status status) {
		return players.stream()
				.map(RequestPlayer::requests)
				.flatMap(Collection::stream)
				.filter(request -> request.getStatus().equals(status))
				.count();
	}

	/**
	 * Null-safe version of {@link Status#valueOf(String)}.
	 *
	 * @param name The name to be mapped.
	 * @return Matching {@link Status} if found, {@code null} otherwise.
	 */
	@Nullable
	public Status mapNameToStatus(@NotNull final String name) {
		try {
			return Status.valueOf(name.toUpperCase());
		} catch (final IllegalArgumentException ex) {
			return null;
		}
	}

	/**
	 * Finds the last used id of any request.
	 *
	 * @return The highest id that has been used by any {@link Request}.
	 */
	public long getLastUsedId() {
		return players.stream()
				.map(RequestPlayer::requests)
				.flatMap(Collection::stream)
				.mapToLong(Request::getId)
				.max()
				.orElse(0);
	}

	/**
	 * Getter for the list of players.
	 *
	 * @return The list of players.
	 */
	@NotNull
	public List<RequestPlayer> getPlayers() {
		return players;
	}

	/**
	 * Finds the {@link Request} of the given id and creates the {@link PlayerRequestPair} with its owner.
	 *
	 * @param id The id of the request to find.
	 * @return The {@link PlayerRequestPair} of the {@link RequestPlayer} and its {@link Request} with the given id.
	 */
	@Nullable
	public PlayerRequestPair getPlayerRequestPair(final int id) {
		return getPlayers().stream()
				.flatMap(requestPlayer -> requestPlayer.requests().stream()
						.filter(request -> request.getId() == id)
						.map(request -> new PlayerRequestPair(requestPlayer, request)))
				.findFirst()
				.orElse(null);
	}
}
