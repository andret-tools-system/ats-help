/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.request;

import org.jetbrains.annotations.NotNull;

/**
 * The record that stores a related {@link RequestPlayer} with a {@link Request} that belongs to it.
 *
 * @param requestPlayer The owner.
 * @param request       The request.
 * @author Andret2344
 * @since Jan 24, 2021
 */
public record PlayerRequestPair(@NotNull RequestPlayer requestPlayer, @NotNull Request request) {
}
