/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.request;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * The request class.
 *
 * @author Andret2344
 * @since Feb 13, 2021
 */
public class Request {
	private final long id;
	@NotNull
	private final String message;
	@NotNull
	private Status status;

	public Request(final long id, @NotNull final String message, @NotNull final Status status) {
		this.id = id;
		this.message = message;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	@NotNull
	public String getMessage() {
		return message;
	}

	@NotNull
	public Status getStatus() {
		return status;
	}

	public void setStatus(@NotNull final Status status) {
		this.status = status;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Request request = (Request) o;
		return getId() == request.getId()
				&& Objects.equals(getMessage(), request.getMessage())
				&& getStatus() == request.getStatus();
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getMessage(), getStatus());
	}

	@Override
	public String toString() {
		return "Request{" +
				"id=" + id +
				", message='" + message + '\'' +
				", status=" + status +
				'}';
	}
}
