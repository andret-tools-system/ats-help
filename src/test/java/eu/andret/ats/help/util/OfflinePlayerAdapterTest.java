/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import eu.andret.ats.help.HelpPlugin;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OfflinePlayerAdapterTest {
	@Test
	void testSerialize() {
		// given
		final UUID uuid = UUID.randomUUID();
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final JsonSerializationContext context = mock(JsonSerializationContext.class);
		final OfflinePlayer offlinePlayer = mock(OfflinePlayer.class);
		when(offlinePlayer.getUniqueId()).thenReturn(uuid);

		final OfflinePlayerAdapter offlinePlayerAdapter = new OfflinePlayerAdapter(plugin);

		// when
		final JsonElement element = offlinePlayerAdapter.serialize(offlinePlayer, OfflinePlayerAdapter.class, context);

		// then
		assertThat(element.getAsString()).isEqualTo(uuid.toString());
	}

	@Test
	void testDeserialize() {
		// given
		final UUID uuid = UUID.randomUUID();
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final JsonDeserializationContext context = mock(JsonDeserializationContext.class);
		final OfflinePlayer offlinePlayer = mock(OfflinePlayer.class);
		final Server server = mock(Server.class);
		when(plugin.getServer()).thenReturn(server);
		when(server.getOfflinePlayer(uuid)).thenReturn(offlinePlayer);

		final OfflinePlayerAdapter offlinePlayerAdapter = new OfflinePlayerAdapter(plugin);
		final JsonElement element = new JsonPrimitive(uuid.toString());

		// when
		final OfflinePlayer result = offlinePlayerAdapter.deserialize(element, OfflinePlayerAdapter.class, context);

		// then
		assertThat(result).isSameAs(offlinePlayer);
	}
}
